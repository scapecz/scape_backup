# s-cape backup

This programs allows backing up django project from production server to backup server using ssh, scp and rsync.

## Installation

- `git clone git@bitbucket.org:scapecz/scape_backup.git`
- `cd scape_backup`
- `virtualenv env`
- `. env/bin/activate`
- `pip install -r requirements.txt`


## Configuration

- create `settings.py` from template `settings.template.py`
- add to `settings.py` projects you want to backup
- setup user and authorized_keys on backup server
- !connect to backup server using ssh credentials for selected project to add it to known hosts `ssh <xxyy>@backup.s-cape.cz`

- test project is set up correctly by running `./run_backup.sh <project_name> --test`
- set up cron for backups that will run `./run_backup.sh <project_name>`
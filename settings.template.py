PROJECTS = {
    'test': {
        'name': 'backup_test',  # name of the project
        'path': '/home/backup_test/www/',  # path to root of the project that should be backuped
        'data': 'deploy/data',  # relative path to data folder
        'db_names_postgres': ['backup_test', ],  # database names that should be backuped
        'exclude': "--exclude *.pyc --exclude deploy/data --exclude deploy/env --exclude .git",  # rsync format; files and folders to exclude when backing up sources
        'stored_backups': 10,
        'backup_ssh': 'backup_test@backup.s-cape.cz',
        'backup_dir': '/home/backup_test/backups/'
    },
}

DB_DUMP_USER = 'postgres'  # None to use current, on servers probably set to postgres
TEMP_DIR = '/var/backup_temp/'  # root temp directory, has to be accessible by DB_DUMP_USER
EMAIL_HOST = 'smtp.rozhled.cz'
EMAIL_HOST_USER = 'server@s-cape.cz'
EMAIL_HOST_PASSWORD = 'r0b0t'

ADMINS = ['michal.majsky@s-cape.cz']  # recipients of error emails
SEND_EMAILS = True  # whether send error emails

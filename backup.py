#!/usr/bin/env python
import argparse
from subprocess import check_call, CalledProcessError
import paramiko
import traceback
import os
from datetime import datetime
from termcolor import cprint
import logging
import shutil
from logging.handlers import RotatingFileHandler, SMTPHandler

import settings

DATETIME_FORMAT = "%Y-%m-%d_%H.%M.%S"
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))


class ProjectBackup:
    def __init__(self, project_settings):
        self.backup_time_str = datetime.now().strftime(DATETIME_FORMAT)
        self.settings = project_settings
        self.logger = self._setup_logging()
        try:
            self.ssh_client = self._create_ssh_connection()
        except Exception as e:
            self.logger.exception('Problem connecting via ssh')
            raise e
        self.temp_dir = os.path.join(settings.TEMP_DIR, self.settings['name'])
        self.root_backup_dir = os.path.join(self.settings['backup_dir'])
        self.backup_dir = os.path.join(self.root_backup_dir, 'backup_' + self.backup_time_str)

    def _setup_logging(self):
        logger = logging.getLogger(self.settings['name'])
        logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        fh = RotatingFileHandler(filename=os.path.join(PROJECT_ROOT, 'log', self.settings['name'] + '.log'), maxBytes='1MB', backupCount=10)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(formatter)
        logger.addHandler(fh)
        if settings.SEND_EMAILS:
            smtph = SMTPHandler(mailhost=settings.EMAIL_HOST, fromaddr='backup@s-cape.cz', toaddrs=settings.ADMINS, subject='Backup error',
                                credentials=(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD))
            smtph.setLevel(logging.ERROR)
            logger.addHandler(smtph)
        return logger

    def _print(self, text):
        self.logger.info(text)
        cprint(text, 'yellow')

    def _print_error(self, text):
        self.logger.error(text)
        cprint(text, 'red')

    def _print_success(self, text):
        self.logger.info(text)
        cprint(text, 'green')

    def _create_temp_dir(self):
        if not os.path.exists(self.temp_dir):
            os.makedirs(self.temp_dir)
        root_temp_dir = os.path.join(settings.TEMP_DIR)
        self._run_local('chmod -R 777 {temp_dir}'.format(temp_dir=root_temp_dir))

    def _create_ssh_connection(self):
        username, host = self.settings['backup_ssh'].split("@")
        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(host, username=username, timeout=10)
        return client

    def cleanup(self):
        self._print('Cleaning up')
        if os.path.exists(self.temp_dir):
            shutil.rmtree(self.temp_dir)
        self.ssh_client.close()

    def _cleanup_and_raise_error(self, error):
        self._run_ssh('rm -rf {backup_dir}'.format(backup_dir=self.backup_dir))
        self.cleanup()
        self.logger.error(error)
        self._print_error(error)
        raise Exception(error)

    def _run_ssh(self, command):
        stdin, stdout, stderr = self.ssh_client.exec_command(command)
        exit_status = stdout.channel.recv_exit_status()  # Blocking call
        if exit_status == 1:
            self._cleanup_and_raise_error('Aborting, failed command: %s: %s' % (command, stderr.read()))
        return stdin, stdout, stderr

    def _run_local(self, command):
        if isinstance(command, basestring):
            command = command.split(" ")
        try:
            check_call(command)
        except CalledProcessError as exc:
            tb = traceback.format_exc()
            self._cleanup_and_raise_error(tb)

    def _create_backup_dir(self):
        self._run_ssh('mkdir -p {backup_dir}'.format(backup_dir=self.backup_dir))
        self._print('Checked or created backup directory')

    def delete_old_backups(self):
        self._print('Deleting old backups')
        stdin, stdout, stderr = self._run_ssh('ls -d {root_backup_dir}/backup_*/'.format(root_backup_dir=self.root_backup_dir))
        output = stdout.read()
        dirs = output.splitlines()
        dir_dates = []
        for dir_name in dirs:
            dir_date_str = dir_name.split('/')[-2].split('backup_')[1]
            try:
                dir_date = datetime.strptime(dir_date_str, DATETIME_FORMAT)
                dir_dates.append([dir_name, dir_date])
            except ValueError:
                pass  # unrecognized folder started with backup_
        sorted_dirs = sorted(dir_dates, key=lambda item: item[1], reverse=True)
        backups_to_delete = sorted_dirs[self.settings['stored_backups']:]
        for backup in backups_to_delete:
            self._run_ssh('rm -rf {backup_path}'.format(backup_path=backup[0]))
        self._print('Successfully deleted {count} old backups'.format(count=len(backups_to_delete)))

    def backup_databases(self):
        self._create_temp_dir()
        self._create_backup_dir()

        for db_name in self.settings['db_names_postgres']:
            self._print('Dumping database {db_name}'.format(db_name=db_name))
            dump_name = "{db_name}.sql".format(db_name=db_name)
            dump_path = os.path.join(self.temp_dir, dump_name)
            cmd = "pg_dump {db_name} -f {dump_path}".format(db_name=db_name, dump_path=dump_path)
            if settings.DB_DUMP_USER:
                cmd = ['su', settings.DB_DUMP_USER, '-c', cmd]
            self._run_local(cmd)
            self._print('Transfering database dump file')
            self._run_local('scp {dump_path} {ssh}:{backup_dir}'.format(ssh=self.settings['backup_ssh'], dump_path=dump_path, backup_dir=self.backup_dir))
        self._print_success('Database backup finished')

    def backup_data(self):
        if self.settings['data']:
            self._print('Backuping data')
            self._create_backup_dir()
            cmd = "rsync -aPv {exclude} {data_path} {ssh}:{backup_dir}".format(
                ssh=self.settings['backup_ssh'],
                data_path=os.path.join(self.settings['path'], self.settings['data'], ),
                backup_dir=os.path.join(self.backup_dir, '../'),
                data_dir=self.settings['data'],
                exclude=self.settings['data_exclude'] if 'data_exclude' in self.settings else '',
            )
            self._run_local(cmd)
            self._run_ssh('echo {timestamp} > {timestamp_file}'.format(timestamp=self.backup_time_str, timestamp_file=os.path.join(self.backup_dir, '../', 'last_data_sync.txt')))
            self._print_success('Backuping data finished')
        else:
            self._print('Skipping backing up data - path not set')

    def backup_sources(self):
        self._print('Backuping sources')
        self._create_backup_dir()
        source_dir = os.path.join(self.backup_dir, 'sources')
        self._run_ssh('mkdir -p {source_dir}'.format(source_dir=source_dir))
        cmd = "rsync -avz {exclude} {project_path} {ssh}:{source_dir}".format(
            ssh=self.settings['backup_ssh'],
            project_path=self.settings['path'],
            source_dir=source_dir,
            exclude=self.settings['exclude'],
        )
        self._run_local(cmd)
        self._print_success('Backuping sources finished')

    def run_backup(self):
        self.test_settings()
        self._print('Starting backup')
        self.backup_databases()
        self.backup_data()
        self.backup_sources()
        self.delete_old_backups()
        self.cleanup()
        self._print_success('Backup finished')

    def test_settings(self):
        for key in ['name', 'path', 'data', 'db_names_postgres', 'exclude', 'stored_backups']:
            if key not in self.settings:
                self._cleanup_and_raise_error('Improperly Configured: Key "{key}" doesnt exist in project settings'.format(key=key))
        for db_name in self.settings['db_names_postgres']:
            # cmd = ['psql', db_name,]
            if settings.DB_DUMP_USER:
                cmd = ['su', settings.DB_DUMP_USER, '-c', "psql {db_name} -c \"select 1+1\"".format(db_name=db_name)]
            else:
                cmd = ['psql', db_name, '-c', "select 1+1"]
            self._run_local(cmd)
            self._print('OK {db_name} connection works'.format(db_name=db_name))
        if not os.path.exists(self.settings['path']):
            self._cleanup_and_raise_error('Project path {path} doesnt exist'.format(path=self.settings['path']))
        self._print('OK {path} exists'.format(path=self.settings['path']))
        if self.settings['data']:
            if not os.path.exists(os.path.join(self.settings['path'], self.settings['data'])):
                self._cleanup_and_raise_error('Project Data path {path} doesnt exist'.format(path=self.settings['data']))
            self._print('OK data dir {dir} exists'.format(dir=self.settings['data']))
        else:
            self._print('Warning: Data dir is not set, No data will be backuped')
        self._print_success('Settings test successful')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('project', type=str)
    parser.add_argument('-t', '--test', dest='test', default=False, const=True, action='store_const')
    parser.add_argument('--database', dest='database', default=False, const=True, action='store_const')
    parser.add_argument('--sources', dest='sources', default=False, const=True, action='store_const')
    parser.add_argument('--data', dest='data', default=False, const=True, action='store_const')

    args = parser.parse_args()
    project_backup = ProjectBackup(settings.PROJECTS[args.project])
    if args.test:
        project_backup.test_settings()
    elif args.database:
        project_backup.backup_databases()
    elif args.sources:
        project_backup.backup_sources()
    elif args.data:
        project_backup.backup_data()
    else:
        project_backup.run_backup()
